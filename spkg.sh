#!/bin/bash
#
# Author: HuangJiayao
#

# Environment
THIS_FILE_NAME=$0
ALL_ARGV=$@
TMP_DIR=`mktemp --directory /tmp/spkg-tmp.XXXXXXXX`
WORK_DIR=${TMP_DIR}
CURRENT_DIR=`pwd`
SCRIPT_END_LINE=`awk '/^__ARCHIVE_BELOW__/ {print NR; exit 0; }' "$0"`
ARCHIVE_BEGIN=`echo $(( ${SCRIPT_END_LINE} + 1 ))`

function pack() {
	if [[ ! -d ${1} ]]; then
		echo "ERROR: ${1} is not a directory, abort."
		exit 1
	fi
	PROJECT_NAME=`echo ${1} | sed 's/\/$//' | awk -F'/' '{print $NF}'`
	head -n ${SCRIPT_END_LINE} ${THIS_FILE_NAME} > ${CURRENT_DIR}/${PROJECT_NAME}.sh

	echo "INFO: Start package files."
	mkdir -p ${WORK_DIR}/src
	cp -a ${1} ${WORK_DIR}/src

	cd ${WORK_DIR}/src

	# Add a entry script, which will be executed after unpack.
	echo "#!/bin/bash" > ${PROJECT_NAME}/spkg-entry.sh
	echo "#" >> ${PROJECT_NAME}/spkg-entry.sh
	echo "# This script will be executed after unpack." >> ${PROJECT_NAME}/spkg-entry.sh
	echo "# Please write the action you want to do below." >> ${PROJECT_NAME}/spkg-entry.sh
	echo "#" >> ${PROJECT_NAME}/spkg-entry.sh
	echo "echo \"Hello SPKG\"" >> ${PROJECT_NAME}/spkg-entry.sh
	echo "echo \"Got argvs: \$@\"" >> ${PROJECT_NAME}/spkg-entry.sh
	chmod +x ${PROJECT_NAME}/spkg-entry.sh

	# Find a editer, and open the entry script.
	if [[ -n `which vim 2> /dev/null` ]]; then
		vim ${PROJECT_NAME}/spkg-entry.sh
	elif [[ -n `which nano 2> /dev/null` ]]; then
		nano ${PROJECT_NAME}/spkg-entry.sh
	elif [[ -n `which vi 2> /dev/null` ]]; then
		vi ${PROJECT_NAME}/spkg-entry.sh
	elif [[ -n `which emacs 2> /dev/null` ]]; then
		emacs ${PROJECT_NAME}/spkg-entry.sh
	else
		echo "ERROR: Can't find a editer. Please install vim."
		exit 1;
	fi

	tar -zcf ${WORK_DIR}/${PROJECT_NAME}.tar.gz ${PROJECT_NAME}
	cd - > /dev/null

	cat ${WORK_DIR}/${PROJECT_NAME}.tar.gz >> ${CURRENT_DIR}/${PROJECT_NAME}.sh
	chmod +x ${CURRENT_DIR}/${PROJECT_NAME}.sh
	echo "INFO: Package finished."
}

function unpack() {
	if [[ -z ${UNPACK_DIR} ]]; then
		UNPACK_DIR="${WORK_DIR}/files"
	fi
	if [[ ! -d ${UNPACK_DIR} ]]; then
		mkdir -p ${UNPACK_DIR}
	fi
	echo "INFO: Unpacking file to ${UNPACK_DIR}"
	tail -n+${ARCHIVE_BEGIN} "${THIS_FILE_NAME}" | tar -zx -C ${UNPACK_DIR} 2> /dev/null
	if [[ $? -ne 0 ]]; then
		echo "ERROR: Bad package file, quit..."
		exit 1
	else
		echo "INFO: Unpack finished"
	fi
	return 0
}

function run() {
	UNPACK_DIR="${WORK_DIR}/files"
	unpack
	PROJECT_NAME=`echo ${THIS_FILE_NAME} | sed 's/.sh$//' | awk -F'/' '{print $NF}'`
	echo "INFO: Run project: ${PROJECT_NAME}"

	# Setup base environment.
	export PATH=${UNPACK_DIR}/${PROJECT_NAME}:${PATH}
	if [[ -d "${UNPACK_DIR}/${PROJECT_NAME}/lib" ]]; then
		export LD_LIBRARY_PATH=${UNPACK_DIR}/${PROJECT_NAME}/lib:${LD_LIBRARY_PATH}
	fi

	cd ${UNPACK_DIR}/${PROJECT_NAME}
	spkg-entry.sh ${ALL_ARGV}
}

if [[ "${1}" == "spkg-package" ]]; then
	pack ${2}
elif [[ "${1}" == "spkg-unpackage" ]]; then
	UNPACK_DIR=${CURRENT_DIR}
	unpack
else
	run
fi

# Do some clean
rm -rf ${TMP_DIR}

# Exit now
exit 0

#This line must be the last line of the file
__ARCHIVE_BELOW__
